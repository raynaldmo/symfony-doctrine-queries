-- MySQL dump 10.13  Distrib 5.6.35, for osx10.9 (x86_64)
--
-- Host: localhost    Database: doctrine_queries
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `iconKey` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Job','fa-dollar'),(2,'Lunch','fa-spoon'),(3,'Proverbs','fa-quote-left'),(4,'Pets','fa-paw'),(5,'Love','fa-heart'),(6,'Lucky Number','fa-bug'),(7,'Test','fa-test');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fortune_cookie`
--

DROP TABLE IF EXISTS `fortune_cookie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fortune_cookie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `fortune` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `numberPrinted` int(11) NOT NULL,
  `discontinued` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F8D8B48712469DE2` (`category_id`),
  CONSTRAINT `FK_F8D8B48712469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fortune_cookie`
--

LOCK TABLES `fortune_cookie` WRITE;
/*!40000 ALTER TABLE `fortune_cookie` DISABLE KEYS */;
INSERT INTO `fortune_cookie` VALUES (1,1,'It would be best to maintain a low profile for now.','2016-06-28 17:50:26',63339,1),(2,1,'404 Fortune not found. Abort, Retry, Ignore?','2014-09-29 03:40:55',41213,0),(3,1,'404 Fortune not found. Abort, Retry, Ignore?','2019-09-14 17:49:08',9532,1),(4,1,'It would be best to maintain a low profile for now.','2015-01-30 11:18:23',31743,1),(5,2,'You will be hungry for chicken.','2018-09-25 21:14:44',31921,1),(6,2,'You will be hungry again in one hour.','2016-10-14 17:45:31',46393,1),(7,2,'You will be hungry again in one hour.','2017-07-22 20:15:10',22276,0),(8,2,'You will be hungry again in one hour.','2018-06-15 21:53:41',87854,1),(9,3,'A conclusion is simply the place where you got tired of thinking.','2016-07-12 19:14:28',45403,1),(10,3,'When you squeeze an orange, orange juice comes out. Because that\'s what\'s inside.','2018-11-02 03:21:32',60766,0),(11,3,'When you squeeze an orange, orange juice comes out. Because that\'s what\'s inside.','2018-09-21 16:41:36',48129,0),(12,4,'That wasn\'t chicken','2017-05-22 20:50:07',75445,0),(13,4,'That wasn\'t chicken','2019-01-07 23:11:30',52664,0),(14,5,'Are your legs tired? You\'ve been running through someone\'s mind all day long.','2016-04-21 00:54:38',86020,1),(15,5,'An alien of some sort will be appearing to you shortly!','2017-11-04 18:48:19',2394,1),(16,5,'run','2018-02-11 06:33:56',46799,0),(17,6,'10^2','2015-02-11 08:56:19',85873,0),(18,6,'Jar Jar Binks','2018-11-26 19:45:46',95048,1),(19,6,'Jar Jar Binks','2018-03-04 13:43:14',77742,0),(20,6,'42','2017-03-20 13:59:17',11659,0),(21,6,'42','2019-05-01 00:26:37',49132,1);
/*!40000 ALTER TABLE `fortune_cookie` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-13 19:08:36
